import React, { Component } from 'react';
import './css/StudentRegister.css';
import _ from 'lodash';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { Tabs, Tab } from 'react-bootstrap';
import StudentEnrolledCourse from './StudentEnrolledCourse';
import ScoreAndGrade from './ScoreAndGrade';

class Student extends Component {
    constructor(props) {
        super(props);
        this.state = {
            enrolledActivity: [],
            key: 'student',
            selectedRow: [],
        };
    }

    componentDidMount() {
        const {
            loadActivitiesFromAdmin,
            userId,
            loadAllEnrolledActivities,
        } = this.props;
        loadActivitiesFromAdmin();
        loadAllEnrolledActivities(userId);
    }

    _setKey = (key) => {
        return this.setState({ key: key });
    };
    _onSelectRow(row, isSelected) {
        let selectRows = _.cloneDeep(this.state.selectedRow);
        if (!isSelected) {
            selectRows.pop(row);
        } else {
            selectRows.push(row);
        }
        this.setState({
            selectedRow: selectRows,
        });
        this._activityEnrollmentHelper(selectRows);

        console.log(`is selected: ${isSelected}, ${selectRows}`);
    }

    _onSelectAll(isSelected, rows) {
        const { selectedRow } = this.state;
        const selectRows = _.concat(selectedRow, rows);
        this.setState({
            selectedRow: !isSelected ? [] : selectRows,
        });
        this._activityEnrollmentHelper(selectRows);
        console.log('rows', selectedRow);
    }

    _activityEnrollmentHelper = (courses: Object) => {
        const { enrolledActivity } = this.state;
        _.remove(enrolledActivity);
        _.forEach(courses, (row) => {
            const activity = _.assign({
                id: row.id,
                activity_id: row.activity_id,
                activity_name: row.activity_name,
                activity_credit: row.activity_credit,
                num_hour: row.num_hour,
                semester: row.semester,
                academic_year: row.academic_year,
                instructor: row.instructor,
                max_enrollment: row.max_enrollment,
                activity_status: row.activity_status,
            });

            enrolledActivity.push(activity);
        });
        console.log(enrolledActivity);
    };

    _confirmActivityEnrollment(activity: Object) {
        const { enrollActivity, userId, enrolled_activity } = this.props;
        const { enrolledActivity } = this.state;
        let helperList = [];
        _.remove(helperList);
        let helperList2 = [];
        _.remove(helperList2);
        if (window.confirm('Are you sure you wish to these activity?')) {
            if (!_.isEmpty(enrolled_activity)) {
                _.forEach(enrolled_activity, (value) => {
                    let hour = _.toNumber(value.num_hour);
                    helperList.push(hour);
                });
            }
            if (!_.isEmpty(enrolledActivity)) {
                _.forEach(enrolledActivity, (value) => {
                    let hour = _.toNumber(value.num_hour);
                    helperList2.push(hour);
                });
            }
            const enrolldHourSum = _.sum(helperList);
            const toEnrollHourSum = _.sum(helperList2);
            console.log('enrolldHourSum', enrolldHourSum);
            console.log('toEnrollHourSum', toEnrollHourSum);
            const sumAll = enrolldHourSum + toEnrollHourSum;
            console.log('sumAll', sumAll);
            if (sumAll <= 50) {
                enrollActivity(userId, activity);
            } else {
                alert(
                    'Sorry, you can enroll activity more than 50 hours per semester!'
                );
            }
        }
    }

    _createCustomDeleteButton = (openModal) => {
        const { enrolledActivity } = this.state;
        return (
            <button
                style={{ backgroundColor: '#F4A460', color: 'White' }}
                disabled={_.isEmpty(enrolledActivity)}
                onClick={() =>
                    this._confirmActivityEnrollment(enrolledActivity)
                }
            >
                Enroll Activity
            </button>
        );
    };

    _renderPaginationPanel = (props) => {
        return (
            <div className="container">
                <div className="row">
                    <div className="col">
                        {props.components.sizePerPageDropdown}
                    </div>
                    <div className="col-md-auto">
                        {props.components.pageList}
                    </div>
                </div>
            </div>
        );
    };
    render() {
        const {
            showToStudentActivityLists,
            loadAllEnrolledActivities,
            enrolled_activity,
            removeEnrolledActivity,
            userId,
        } = this.props;
        const { key } = this.state;

        const selectRowProp = {
            mode: 'checkbox',
            clickToSelect: true,
            bgColor: 'rgb(238, 193, 213)',
            onSelect: this._onSelectRow.bind(this),
            onSelectAll: this._onSelectAll.bind(this),
        };
        const options = {
            paginationPanel: this._renderPaginationPanel,
            deleteBtn: this._createCustomDeleteButton,
        };

        const activityShowList = _.differenceWith(
            showToStudentActivityLists,
            enrolled_activity,
            _.isEqual
        );
        const activityData = _.orderBy(activityShowList, ['id'], ['desc']);
        console.log('enrolled_activity', enrolled_activity);
        console.log('showToStudentActivityLists', showToStudentActivityLists);
        return (
            <div className="register-container">
                <Tabs
                    id="controlled-tab-example"
                    activeKey={key}
                    onSelect={(k) => this._setKey(k)}
                >
                    <Tab eventKey="student" title="Activity">
                        {!_.isEmpty(activityData) ? (
                            <BootstrapTable
                                data={activityData}
                                options={options}
                                selectRow={selectRowProp}
                                deleteRow
                                exportCSV
                                search
                                hover
                                pagination
                                headerStyle={{
                                    background: '#FFF0F5',
                                }}
                            >
                                <TableHeaderColumn
                                    dataField="id"
                                    isKey
                                    hidden
                                    dataAlign="center"
                                >
                                    id
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activity_id"
                                    width="50"
                                    dataAlign="center"
                                >
                                    ID
                                </TableHeaderColumn>

                                <TableHeaderColumn
                                    dataField="activity_name"
                                    width="420"
                                    dataAlign="center"
                                >
                                    Name
                                </TableHeaderColumn>

                                <TableHeaderColumn
                                    dataField="activity_credit"
                                    width="70"
                                    dataAlign="center"
                                    dataSort
                                >
                                    Credit
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="num_hour"
                                    dataSort
                                    dataAlign="center"
                                    width="80"
                                >
                                    Hours
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="max_enrollment"
                                    dataSort
                                    dataAlign="center"
                                    width="90"
                                >
                                    Accept
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="semester"
                                    width="100"
                                    dataAlign="center"
                                >
                                    Semester
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="academic_year"
                                    dataAlign="center"
                                >
                                    Year
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataAlign="center"
                                    dataField="instructor"
                                    width="175"
                                >
                                    Instructor
                                </TableHeaderColumn>
                                <TableHeaderColumn
                                    dataField="activity_status"
                                    dataAlign="center"
                                    hidden
                                >
                                    Status
                                </TableHeaderColumn>
                            </BootstrapTable>
                        ) : (
                            <div className="register-container">
                                <h4>No any activity opened yet !</h4>{' '}
                            </div>
                        )}
                    </Tab>
                    <Tab eventKey="enrolled-activity" title="Enrolled Activity">
                        <StudentEnrolledCourse
                            loadAllEnrolledActivities={
                                loadAllEnrolledActivities
                            }
                            enrolled_activity={enrolled_activity}
                            removeEnrolledActivity={removeEnrolledActivity}
                            userId={userId}
                        />
                    </Tab>
                    <Tab eventKey="enrolled-activity-result" title="Score ">
                        <ScoreAndGrade
                            loadAllEnrolledActivities={
                                loadAllEnrolledActivities
                            }
                            enrolled_activity={enrolled_activity}
                            userId={userId}
                        />
                    </Tab>
                </Tabs>
            </div>
        );
    }
}

export default Student;
