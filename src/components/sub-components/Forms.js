import React, { Component } from 'react';
import { Container, Row } from 'react-bootstrap';
import Logo from '../assets/logo.png';
import './css/Forms.css';

class Forms extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
        };
    }

    _handleChange = (e) => {
        const { name, value } = e.target;

        this.setState({
            [name]: value,
        });
    };

    _handleSubmit = (e) => {
        e.preventDefault();
        const { email, password } = this.state;
        const { userRequestLogin } = this.props;
        userRequestLogin(email, password);
    };

    render() {
        return (
            <Container fluid>
                <Row className="justify-content-center mt-2 ml-5">
                    <div className="jumbotron mt-5">
                        <div className="text-center mb-3 mt-0">
                            <img
                                className="Logo-image rounded"
                                width="90"
                                src={Logo}
                                alr=""
                            />
                        </div>
                        <h5 className="text-center mb-4 mt-4">
                            Activity Registration System
                        </h5>
                        <form
                            className="form-signin"
                            onSubmit={this._handleSubmit}
                        >
                            <input
                                type="email"
                                id="inputEmail"
                                name="email"
                                className="form-control"
                                placeholder="Email"
                                onChange={this._handleChange}
                                required
                            />
                            <input
                                type="password"
                                id="inputPassword"
                                name="password"
                                className="form-control"
                                placeholder="Password"
                                onChange={this._handleChange}
                                required
                            />
                            <button
                                className="btn btn-lg btn-info btn-block mt-4 mb-1"
                                type="submit"
                            >
                                Log in
                            </button>
                        </form>
                    </div>
                </Row>
            </Container>
        );
    }
}
export { Forms };
