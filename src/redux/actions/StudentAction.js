import { db } from '../../utils/Firebase';
import _ from 'lodash';
import firebase from '../../utils/Firebase';
import { USERS_COLLECTION } from '../../utils/constants/firestoreConstant';
import {
    LOAD_ACTIVITY_FROM_ADMIN,
    ENROLL_ACTIVITY,
    LOAD_ALL_ENROLLED_ACTIVITIES,
    REMOVE_ENROLLED_ACTIVITY,
} from '../../utils/constants/constants';
import { history } from '../../system/History';

const firestoreRef = db.collection(USERS_COLLECTION);

export const loadActivitiesFromAdmin = () => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firestoreRef
                .get()
                .then((querySnapshot) => {
                    querySnapshot.forEach((doc) => {
                        const role = doc.data().role;
                        const activityLists = doc.data().activity_lists;
                        if (_.isEqual(role, 'admin')) {
                            let availableList = [];
                            _.forEach(activityLists, (value) => {
                                console.log(value.activity_status);
                                if (
                                    _.isEqual(value.activity_status, 'Opened')
                                ) {
                                    availableList.push(value);
                                }
                            });
                            dispatch({
                                type: LOAD_ACTIVITY_FROM_ADMIN,
                                showToStudentActivityLists: availableList,
                            });
                            console.log(
                                `Load Activities from admin ${activityLists}`
                            );
                        }
                    });
                })
                .catch((error) => {
                    console.warn(error);
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const goTo = (path: String) => {
    return async (dispatch: Function, getState: Function) => {
        history.push(path);
        history.go();
    };
};

export const enrollActivity = (id: string, activity: Object) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            _.forEach(activity, async (row) => {
                await firestoreRef
                    .doc(id)
                    .update({
                        enrolled_activity: firebase.firestore.FieldValue.arrayUnion(
                            {
                                id: row.id,
                                activity_id: row.activity_id,
                                activity_name: row.activity_name,
                                activity_credit: row.activity_credit,
                                num_hour: row.num_hour,
                                semester: row.semester,
                                academic_year: row.academic_year,
                                instructor: row.instructor,
                                max_enrollment: row.max_enrollment,
                                activity_status: row.activity_status,
                            }
                        ),
                    })
                    .then(() => {
                        dispatch({
                            type: ENROLL_ACTIVITY,
                            userId: id,
                        });
                        loadAllEnrolledActivities(id);
                        console.log('Enrolled Successfully');
                    });
            });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const loadAllEnrolledActivities = (id: string) => {
    return async (dispatch: Function, getState: Function) => {
        try {
            await firestoreRef
                .doc(id)
                .onSnapshot((doc) => {
                    const data = doc.data();
                    const enrollActivityList = data.enrolled_activity;
                    dispatch({
                        type: LOAD_ALL_ENROLLED_ACTIVITIES,
                        enrolled_activity: enrollActivityList,
                    });
                })
                .then(() => {
                    console.log('Loading enrolled sucessfully');
                });
        } catch (error) {
            console.warn(error);
        }
    };
};

export const removeEnrolledActivity = (id: string, activity: Object) => {
    return (dispatch: Function, getState: Function) => {
        try {
            _.forEach(activity, async (value) => {
                await firestoreRef
                    .doc(id)
                    .update({
                        enrolled_activity: firebase.firestore.FieldValue.arrayRemove(
                            {
                                academic_year: value.academic_year,
                                activity_credit: value.activity_credit,
                                activity_id: value.activity_id,
                                activity_name: value.activity_name,
                                activity_status: value.activity_status,
                                id: value.id,
                                instructor: value.instructor,
                                max_enrollment: value.max_enrollment,
                                num_hour: value.num_hour,
                                semester: value.semester,
                            }
                        ),
                    })
                    .then(() => {
                        dispatch({
                            type: REMOVE_ENROLLED_ACTIVITY,
                            userId: id,
                        });
                        console.log(
                            `Student Remove Enrolled Activity ${value.activity_name}`
                        );
                    });
            });
        } catch (error) {
            console.warn(error);
        }
    };
};
