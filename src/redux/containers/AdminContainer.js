import { connect } from 'react-redux';
import Admin from '../../components/main-components/Admin';
import {
    addNewActivity,
    loadAllActivities,
    removeActivity,
    updateActivity,
    loadInstructorList,
} from '../actions/UserAction';

const mapStateToProps = (state) => {
    const { user } = state;
    const {
        isUserLoggedIn,
        userRole,
        userId,
        activityLists,
        isModalShow,
        realNameList,
    } = user;

    return {
        isUserLoggedIn,
        userRole,
        userId,
        activityLists,
        isModalShow,
        realNameList,
    };
};
const mapDispatchToProps = (dispatch: Function) => {
    return {
        addNewActivity: (id: string, activity: Object) => {
            dispatch(addNewActivity(id, activity));
        },
        loadAllActivities: (id: string) => {
            dispatch(loadAllActivities(id));
        },
        removeActivity: (id: String, activity: Object) => {
            dispatch(removeActivity(id, activity));
        },
        updatectivity: (id: String, activity: Object) => {
            dispatch(updateActivity(id, activity));
        },
        loadInstructorList: () => {
            dispatch(loadInstructorList());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
