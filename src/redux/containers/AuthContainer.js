import { connect } from 'react-redux';
import { Forms } from '../../components/sub-components/Forms';
import {
    userRequestLoginWithEmailAndPassword,
    userRequestLogout,
} from '../actions/UserAction';

const mapStateToProps = (state) => {
    const { user } = state;
    const { isUserLoggedIn } = user;
    return {
        isUserLoggedIn,
    };
};

const mapDispatchToProps = (dispatch: Function) => {
    return {
        userRequestLogin: (email: string, password: string) => {
            dispatch(userRequestLoginWithEmailAndPassword(email, password));
        },
        userRequestLogout: () => {
            dispatch(userRequestLogout());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Forms);
