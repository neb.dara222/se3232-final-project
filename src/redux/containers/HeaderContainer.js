import { connect } from 'react-redux';
import Header from '../../components/sub-components/Header';
import { userRequestLogout } from '../actions/UserAction';

const mapStateToProps = (state) => {
    const { user } = state;
    const { isUserLoggedIn, userId, userRole, realName } = user;
    return {
        isUserLoggedIn,
        userId,
        userRole,
        realName,
    };
};
const mapDispatchToProps = (dispatch: Function) => {
    return {
        userRequestLogout: () => {
            dispatch(userRequestLogout());
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
