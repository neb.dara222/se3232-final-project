import { connect } from 'react-redux';
import InsertModal from '../../components/sub-components/InsertModal';

const mapStateToProps = (state: Object) => {
    const { user } = state;
    const { isModalShow } = user;

    return {
        isModalShow,
    };
};

export default connect(mapStateToProps, null)(InsertModal);
