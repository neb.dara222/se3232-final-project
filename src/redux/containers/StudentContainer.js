import { connect } from 'react-redux';
import Student from '../../components/main-components/Student';
import {
    loadActivitiesFromAdmin,
    enrollActivity,
    loadAllEnrolledActivities,
    removeEnrolledActivity,
} from '../actions/StudentAction';

const mapStateToProps = (state) => {
    const { user, student } = state;
    const { isUserLoggedIn, userRole, userId, realName } = user;
    const { showToStudentActivityLists, enrolled_activity } = student;
    return {
        isUserLoggedIn,
        userRole,
        showToStudentActivityLists,
        userId,
        enrolled_activity,
        realName,
    };
};
const mapDispatchToProps = (dispatch: Function) => {
    return {
        loadActivitiesFromAdmin: () => {
            dispatch(loadActivitiesFromAdmin());
        },
        enrollActivity: (id: String, activity: Object) => {
            dispatch(enrollActivity(id, activity));
        },
        loadAllEnrolledActivities: (id: String) => {
            dispatch(loadAllEnrolledActivities(id));
        },
        removeEnrolledActivity: (id: String, activity: Object) => {
            dispatch(removeEnrolledActivity(id, activity));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Student);
