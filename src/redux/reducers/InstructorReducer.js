import {
    LOAD_ENROLLED_STUDENT_LIST,
    GRADE_STUDENT,
    LOAD_GRADED_STUDENT_LIST,
} from '../../utils/constants/constants';

const INITIALIZED_STATE = {
    studentEnrolledList: {},
    graded_student: {},
    userId: '',
};

const loadEnrolledStudentList = (state, action) => {
    return {
        ...state,
        studentEnrolledList: action.studentEnrolledList,
    };
};

const gradeStudent = (state, action) => {
    return {
        ...state,
        userId: action.userId,
    };
};

const loadGradedStudentList = (state, action) => {
    return {
        ...state,
        graded_student: action.graded_student,
    };
};

export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case LOAD_ENROLLED_STUDENT_LIST:
            return loadEnrolledStudentList(state, action);
        case GRADE_STUDENT:
            return gradeStudent(state, action);
        case LOAD_GRADED_STUDENT_LIST:
            return loadGradedStudentList(state, action);
        default:
            return state;
    }
};
