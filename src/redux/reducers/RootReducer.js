import UserReducer from './UserReducer';
import SignUpReducer from './SignUpReducer';
import StudentReducer from './StudentReducer';
import InstructorReducer from './InstructorReducer';

const RootReducer = {
    user: UserReducer,
    signup: SignUpReducer,
    student: StudentReducer,
    instructor: InstructorReducer,
};

export default RootReducer;
