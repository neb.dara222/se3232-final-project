import {
    LOAD_ACTIVITY_FROM_ADMIN,
    ENROLL_ACTIVITY,
    LOAD_ALL_ENROLLED_ACTIVITIES,
    REMOVE_ENROLLED_ACTIVITY,
} from '../../utils/constants/constants';

const INITIALIZED_STATE = {
    showToStudentActivityLists: {},
    userId: '',
    enrolled_activity: {},
};

const loadActivitiesFromAdmin = (state, action) => {
    return {
        ...state,
        showToStudentActivityLists: action.showToStudentActivityLists,
    };
};
const enrollActivity = (state, action) => {
    return {
        ...state,
        userId: action.userId,
    };
};
const loadAllEnrolledActivities = (state, action) => {
    return {
        ...state,
        enrolled_activity: action.enrolled_activity,
    };
};
const removeEnrolledActivity = (state, action) => {
    return {
        ...state,
        userId: action.userId,
    };
};
export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case LOAD_ACTIVITY_FROM_ADMIN:
            return loadActivitiesFromAdmin(state, action);
        case ENROLL_ACTIVITY:
            return enrollActivity(state, action);
        case LOAD_ALL_ENROLLED_ACTIVITIES:
            return loadAllEnrolledActivities(state, action);
        case REMOVE_ENROLLED_ACTIVITY:
            return removeEnrolledActivity(state, action);
        default:
            return state;
    }
};
