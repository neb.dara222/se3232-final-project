import { USER_REQUEST_LOGIN, USER_REQUEST_LOGOUT } from '../actions/ActionList';
import {
    ADD_NEW_ACTIVITY,
    LOAD_ALL_ACTIVITIES,
    REMOVE_ACTIVITY,
    UPDATE_ACTIVITY,
    LOAD_INSTRUCTOR_LIST,
} from '../../utils/constants/constants';

const INITIALIZED_STATE = {
    isUserLoggedIn: false,
    userId: '',
    userRole: '',
    activityLists: {},
    isModalShow: false,
    realName: '',
    realNameList: [],
};

const userRequestLogin = (state, action) => {
    return {
        ...state,
        isUserLoggedIn: true,
        userId: action.userId,
        userRole: action.userRole,
        activityLists: action.activityLists,
        realName: action.realName,
    };
};

const userRequestLogout = (state, action) => {
    return {
        ...state,
        isUserLoggedIn: false,
        userId: action.userId,
        userRole: action.userRole,
        activityLists: action.activityLists,
        realName: action.realName,
        realNameList: action.realNameList,
    };
};

const addNewActivity = (state, action) => {
    return {
        ...state,
        userId: action.userId,
    };
};

const loadAllActivities = (state, action) => {
    return {
        ...state,
        activityLists: action.activityLists,
        realName: action.realName,
    };
};

const removeActivity = (state, action) => {
    return {
        ...state,
        userId: action.userId,
    };
};
const updateActivity = (state, action) => {
    return {
        ...state,
        userId: action.userId,
    };
};

const loadInstructorList = (state, action) => {
    return {
        ...state,
        realNameList: action.realNameList,
    };
};
export default (state = INITIALIZED_STATE, action) => {
    switch (action.type) {
        case USER_REQUEST_LOGIN:
            return userRequestLogin(state, action);
        case USER_REQUEST_LOGOUT:
            return userRequestLogout(state, action);
        case ADD_NEW_ACTIVITY:
            return addNewActivity(state, action);
        case LOAD_ALL_ACTIVITIES:
            return loadAllActivities(state, action);
        case REMOVE_ACTIVITY:
            return removeActivity(state, action);
        case UPDATE_ACTIVITY:
            return updateActivity(state, action);
        case LOAD_INSTRUCTOR_LIST:
            return loadInstructorList(state, action);
        default:
            return state;
    }
};
