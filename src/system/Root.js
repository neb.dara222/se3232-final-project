import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import _ from 'lodash';
import AuthContainer from '../redux/containers/AuthContainer';
import HeaderContainer from '../redux/containers/HeaderContainer';
import {
    ADMIN_ROLE,
    STUDENT_ROLE,
    INSTRUCTOR_ROLE,
} from '../utils/constants/constants';
import AdminContainer from '../redux/containers/AdminContainer';
import StudentContainer from '../redux/containers/StudentContainer';
import InstructorContainer from '../redux/containers/InstructorContainer';
import StudentCourseContainer from '../redux/containers/StudentCourseContainer';

class Root extends Component {
    _renderContainer(userRole) {
        switch (userRole) {
            case ADMIN_ROLE:
                return <AdminContainer />;
            case STUDENT_ROLE:
                return <StudentContainer />;
            case INSTRUCTOR_ROLE:
                return <InstructorContainer />;
            default:
                return <AuthContainer />;
        }
    }
    render() {
        const { isUserLoggedIn, userRole, userId } = this.props;
        return (
            <Router>
                {isUserLoggedIn && !!userRole && !!userId ? (
                    <HeaderContainer />
                ) : null}
                <Switch>
                    <Route
                        exact
                        path="/*"
                        render={() => this._renderContainer(userRole)}
                    />
                    <Route
                        path={userRole}
                        render={() => this._renderContainer(userRole)}
                    />
                    <Route path="/home" component={StudentCourseContainer} />
                </Switch>
            </Router>
        );
    }
}

export default Root;
