import { connect } from 'react-redux';
import Root from './Root';

const mapStateToProps = (state) => {
    const { user } = state;
    const { isUserLoggedIn, userId, userRole } = user;

    return {
        isUserLoggedIn,
        userId,
        userRole,
    };
};

export default connect(mapStateToProps, null)(Root);
